<?php 
header('Access-Control-Allow-Origin: https://app.ticketmaster.com/discovery/v2/events.json? | https://app.ticketmaster.com'); 
header('Access-Control-Allow-Credentials: true');
?>
<link rel="shorcut icon" type="image/x-icon" href="view/img/ticket.png")
<?php

if (isset($_GET['page'])){
    switch($_GET['page']){
        case "controller_entrada";
            include("view/inc/top_page_entradas.php");
            break; 
        case "controller_events";
            include("view/inc/top_page_events.php");
            break;  
        case "controller_login";
            include("view/inc/top_page_login.php");
            break;
        case "controller_cart";
            include("view/inc/top_page_cart.php");
            break;
        case "controller_filter";
            include("view/inc/top_page_filter.php");
            break; 
        case "controller_profile";
            include("view/inc/top_page_profile.php");
            break;
        default;
            include("view/inc/top_page.php");
            break;
    }
}else{
        include("view/inc/top_page.php");
    }
/*
    if ((isset($_GET['page'])) && ($_GET['page']==="controller_entrada")){
		include("view/inc/top_page_entradas.php");
	}else if ((isset($_GET['page'])) && ($_GET['page']==="controller_events")){
        include("view/inc/top_page_events.php");
    }else if ((isset($_GET['page'])) && ($_GET['page']==="controller_login")){
        include("view/inc/top_page_login.php");
    }else{
		include("view/inc/top_page.php");
	}*/

	session_start();
    include ("model/utils_lenguage/lang.php");
    //print_r($_SESSION);
    //die();
?>

<div id="wrapper">		
    <div id="header">    	
    	<?php
    	    include("view/inc/header.php");
    	?>        
    </div>  
    <div id="menu">
		<?php
        if (isset($_SESSION['user'])){
            if ($_SESSION['type']=="admin")
                include("view/inc/menu/menu_auth_admin.php");
            else 
                include("view/inc/menu/menu_auth.php");
        }else {
            include("view/inc/menu/menu_no_auth.php");
        }

		?>
    </div>	
    <div id="body">
    	<?php 
		include("view/inc/pages.php"); 
		?>   
        <br style="clear:both;" />
      <?php
        include("view/inc/footer2.php"); 
      ?>
    </div>
    <div id="footer">   	   
	    <?php
	        include("view/inc/footer.php");
	    ?>        
    </div>
</div>
<?php
    include("view/inc/bottom_page.php");
?>