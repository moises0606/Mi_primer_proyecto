<?php
	//print_r($_GET);
	//die();
	if (!isset($_GET['page']))
		    $_GET['page'] = ' ';

	switch($_GET['page']){
		case "homepage";
			include("module/inicio/view/inicio.php");
			break;
		case "controller_entrada";
			include("module/entrada/controller/".$_GET['page'].".php");
			break;
		case "services";
			include("module/services/".$_GET['page'].".php");
			break;
		case "aboutus";
			include("module/aboutus/view/".$_GET['page'].".php");
			break;
		case "contactus";
			include("module/contact/".$_GET['page'].".php");
			break;
		case "controller_filter";
			include("module/filter/controller/".$_GET['page'].".php");
			break;	
		case "controller_events";
			include("module/early_events/controller/".$_GET['page'].".php");
			break;	
		case "controller_login";
			include("module/login/controller/".$_GET['page'].".php");
			break;
		case "controller_cart";
			include("module/cart/controller/".$_GET['page'].".php");
			break; 
		case "controller_profile";
			include("module/profile/controller/".$_GET['page'].".php");
			break;
		case "404";
			include("view/inc/error".$_GET['page'].".php");
			break;
		case "503";
			include("view/inc/error".$_GET['page'].".php");
			break;
		default;
			include("module/inicio/view/inicio.php");
			break;
	}
?>