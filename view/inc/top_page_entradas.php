<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Alta de Usuario</title>
      
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>

      <script type="text/javascript">
        	$(function() {
        		$('#date_birthday').datepicker({
        			dateFormat: 'dd-mm-yy', 
        			changeMonth: true, 
        			changeYear: true, 
        			yearRange: '1950:2017',
        			onSelect: function(selectedDate) {
        			}
        		});
            $('#fllegada').datepicker({
              dateFormat: 'dd-mm-yy', 
              changeMonth: true, 
              changeYear: true, 
              yearRange: '0:+2',
              minDate: 0,
              onSelect: function(selectedDate) {
                }
            });
            $('#fsalida').datepicker({
              dateFormat: 'dd-mm-yy', 
              changeMonth: true, 
              changeYear: true, 
              yearRange: '0:+2',
              minDate: 0,
              onSelect: function(selectedDate) {
                }
            });
            });
	    </script>
	    <link href="view/css/style.css" rel="stylesheet" type="text/css" />
	    <script src="module/entrada/model/validate_entrada.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
    </head>
    <body>