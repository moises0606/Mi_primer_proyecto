INTERFAZ--------------------------
Cuando accedemos a la Aplicacion Web, podremos observar que en la parte de arriba
esta el menu, desde aquí podremos acceder a los diferentes modulos dependiendo de que tipo
de usuario seamos (admin, user, guest)
Si somos guest, podremos acceder al HOME, EARLY EVENTS, SERVICES, KNOW US, CONTACT, FILTERS, LOGIN Y CART
Si somos user, podremos acceder a lo mismo que el usuario guest pero podremos realizar compras
Si somos admin podremos acceder a los mismos que el tipo guest pero sin poder acceder al carrito, 
pero tendremos acceso al CRUD que estará en TICKETS

A la parte de bajo de la pagina, veremos el FOOTER, tenemos uno fijo (siempre visible) y otro que lo 
podremos ver si bajamos hacia bajo en la pagina.
----------------------------------

------------------------------HOME
En el home podemos observar a primera vista un carrusel que nos muestra 1 imagen que va cambiando entre diferentes imagenes
Mas abajo podemos ver unos dummies que cogemos desde la base de datos, estos dummies estan mostrados a traves
de bootstrap y pedidos al controlador que hace una solicitud a traves del DAO y los devuelve al controlador,
este devolverá al list_estradas_dummies.php los datos y serán mostrados desde PHP.
Para poder mostrar los dummies, tendremos un include() de list_entradas_dummies.php en inicio.php 
----------------------------------

----------------------EARLY EVENTS
En Early events tenemos la tienda, en la que tenemos 2 dependent dropdowns y un autocomplete.
El 1º DD muestra los tipos de eventos, el 2º DD muestra los eventos dependiendo del tipo de evento, el autocomplete
te ayudará a introducir la provincia
Todos los datos son solicitados a la BBDD, desde JS a traves de POST al controlador i solicitados desde
el DAO, devueltos al controlador y enviados a JS con json_encode.
Cuando tenemos la informacion deseada en los Drop Donwns y el autocomplete hace una peticion al controlador que a traves del
DAO pedirá la entrada con los datos especificados previamente, JS pintara la entrada deseada
y le pedirá a una API otros eventos que le pueden interesar al usuario, de la misma provincia.
Cuando nos ha mostrado JS la entrada deseada, tendremos un botón para añadir al carrito, tantas veces pulsemos
se añadira el producto al carrito, el JS de este botón esta en el JS del carrito.
----------------------------------

-----------------------------LOGIN
LOGIN
	Desde el login podemos loggearnos, introducimos el usuario, y contraseña, JS le pasará el usuario y contraseña
	al controlador a traves de POST, este le pasará la informacion al DAO para comprobar si el usuario existe y si la contraseña es correcta, la respuesta es devuelta al controlador y enviada a JS a traves de json_encode.
	Si el usuario no existe JS pintará el error y en el caso que la constraseña sea incorrecta, mostrará el error.
	Si se ha realizado con exito se guardara en $_SESSION, mostrará un alert y redireccionara al Home.

REGISTER
	Desde el register nos podremos registrar, siempre se realizará como usuario normal (user), JS le pasará la informacion al 
	controlador a traves de POST y este le pasará la informacion al DAO para introducir el usuario la respuesta es devuelta al controlador y se pasa a JS a través de json_encode. Si el usuario o email ya existen, JS mostrará el error
	Si se ha realizado con exito, mostrará un alert y redireccionara al Login.
RECOVER_PASSWORD
	El email se envia a traves de Ajax (POST) al controlador y este verifica con una consulta a la BBDD a traves del DAO 
	la informacion es devuelta al controlador y enviada a JS a traves de json_encode, miestras que JS esta pintando si el email existe 
	o no a traves de JS, en caso de que exista muestra el 2 inputs para poner la contraseña y repetirla, si no coincide, muestra el error, si coincide envia el email y la contraseña a traves de Ajax (POST) al controlador y actualiza la contraseña con una consulta
	desde el DAO, el controlador devolvera si se ha realizado con exito a traves de json_encode, si ha sido un exito, mostrará un alert y redireccionara al Login.
LOGOUT
	Desde el controlador destruiremos todas las sesiones para eliminar el usuario de $_SESSION, al finalizar redigirá al HOME


VERSION EN PHP 		esto fué una version mas antigua (esta en el codigo, pero esta comentado)
	LOGIN
		Al introducir usuario y contraseña se envia al controlador, este realizará una peticion a BBDD a traves del DAO y comprobará si el usuario existe, si este existe comprobará que la contraseña es correcta, si esta es correcta verificará el login guardandolo en $_SESSION y redigirá al HOME.

	REGISTER
		Al introducir usuario, email y contraseña se envia al controlador, este realizará una peticion a BBDD a traves del DAO y comprobará si el usuario o email existen, si alguno de estos existen mostrará el error a traves de php, si no existen
		lo registrará en la BBDD a traves del DAO y al volver al controlador redigirá al LOGIN
----------------------------------

------------------------------CART
Cuando entramos al carrito JS comprobará el LocalStorage, esta vacio mostrara, que este esta vacio junto un boton que nos permitira
redirigirnos a Early Events para comprar, si han sido enviadas entradas desde Early events, se estarán guardando en el en el LocalStorage, si queremos incrementar las entradas podremos pulsar sobre el + para sumar entradas, si pulsamos este botón tambien se incrementará el precio, en caso de pulsar el - se restarán entradas, no podremos bajar de 1, en caso de llegar a este y pulsamos -
JS nos mostrará un botón para eliminar la entrada, tambien se eliminará desde JS del LocalStorage.
Al la derecha podremos ver el precio total de todas las entradas y contando las cantidades de cada una, si pulsamos el botón de checkout, JS empezará a recorrer el carrito de LocalStorage y enviara la informacion necesaria a traves de POST al controlador, en el controlador se comprobará que este un usuario loggeado, si esto no es asi devolvera un error a traves de json_encode y JS redijirá al login. Si estamos loggeados en el controlador enviara una a una las entradas al DAO junto la cantidad y el precio de esta y será registrado en la BBDD si se ha echo correctamente devolvera que se ha echo con exito a traves de json_encode y eliminará esta entrada del LocalStorage, seguira el proceso con tantas entradas diferentes hayan.
Al finalzar mostrara que el carrito esta vacio junto un boton que nos permitira redirigirnos a Early Events para comprar.
----------------------------------

----------------------------KNOW US
Desde aquí el usuario nos conecera un poco mas a traves de Google Maps, le mostramos en un mapa nuestra ubicacion de la SEDE
y tambien le decimos toda la informacion que queramos sobre nosotros la informacion que le mostremos en este caso
es mostramos un Lorem Ipsum
----------------------------------

---------------------------FILTERS
Desde este otro modulo el usuario tendra a disposición un datatable en el cual podrá elegir la cantidad de informacion que
quiere ver o podra filtrarla a traves de un buscador.
La informacion de la tabla son dummies que son pedidos al controlador, solicitados desde el DAO a BBDD y devueltos
a JS para poderlos mostrar, se pasan a traves de un json_encode desde el controlador
----------------------------------

---------------------------TICKETS (CRUD)
LIST
	Si accedemos aquí PHP mostrara pidiendole al controlador (solicitando al DAO y devolviendo la informacion al controlador) todas las entradas.
CREATE
	Cuando pulsemos el botón de crear nos mostrara un formulario para introducir una entrada, cuando pulsamos el boton de comprar comprobara a traves de JS que todos los campos estan llenos si no es asi mostrara un error y desde el controlador enviara la informacion a unos filters en php, si hay algun error se introducirá en un array y se mostrará ya que si hay algun error no se podrá acabar la operacion, la fecha de salida no podrá ser anterior a la de llegada, el dni tendrá que ser valido y no se podrá repetir en la BBDD, la edad esta limitada a 68 años, si todo es correcto, el controlador enviara la informacion al DAO y registrara la entrada
READ JS
	Cuando pulsemos el boton de read, JS cogera la id y se la pasará al controlador, este hará una consulta a traves del DAO y devolvera al controlador tada la informacion del usuario y se enviara a traves de json_encode a JS y este lo mostrará al usuario
	en un modal.
READ PHP 		(esta en el codigo, pero esta comentado)
	Cuando pulsemos el boton de read, le enviaremos la id (cogida a traves de GET) al controlador, este hará una consulta a traves del DAO y devolvera al controlador tada la informacion del usuario se guardará en un array y se redigira al list_entradas.php y este detectara el array realizará un include de read_entrada.php recorrera el array y mostrará al usuario la informacion en un modal.
UPDATE
	Al pulsar el botón mostara una pagina php con toda la informacion del usuario en los apartados correspondientes, la informacion habrá sido soilicitada previamente desde el controlador a traves de GET, cogiendo la id y solicitando toda la informacion al DAO este la devuelve y la introduce en un array para poderla mostrar en la pagina.
		Una vez modificado cuando pulsamos el boton de modificar comprobara a traves de JS que todos los campos estan llenos si no es asi mostrara un error y desde el controlador enviara la informacion a unos filters en php, si hay algun error se introducirá en un array y se mostrará ya que si hay algun error no se podrá acabar la operacion, la fecha de salida no podrá ser anterior a la de llegada, el dni no se podrá modificar a parte tiene que ser valido, y la edad esta limitada a 68 años, si todo es correcto, el controlador enviara la informacion al DAO y registrara la entrada
DELETE
	Al pulsar este boton cogeremos el id a traves de GET y solicitaremos desde el controlador la informacion de este usuario al DAO y devolvera la informacion al controlador, a traves de PHP mostramos si está seguro de borra la informacion del usuario X si pulsamos que si, volvera al controlador y enviara una solicitud de eliminar el usuario en cuestion, si pulsamos cancelar volveremos al list_entradas.php 
----------------------------------

--------------------------SERVICES
Aqui podríamos los tipos de servicios que ofrecemos, en este caso he añadido un Lorem Ipsum para rellenar
----------------------------------

---------------------------CONTACT
Desde este modulo el usuario tendra toda la informacion disponible para poder contactar con nosotros, en este caso
le mostramos Lorem Ipsum
----------------------------------

-----------------------------INDEX
Desde el Index ponemos en el header el Access-Control-Allow-Origin para la API
También decidiremos que top_page realizará un include (se hace para separar librerias)
Dependiendo si estamos logeados o no, mostrará un menu, el guest, user o admin
Por otra parte incluiremos la libreria de idioma que el usuario nos indique, por defecto se carga en inglés
----------------------------------

---------------------------FOOTER1 (Fijo)
Desde este footer tenemos 3 botones que nos redigirán a Contact (Contact number, Email y Direction)
Luego podremos encontrar la lengua en la que queremos tener la aplicacion web, por defecto carga en ingles pero desde aquí podremos cambiar a español (No esta todo en las librerias de lenguaje)
----------------------------------

---------------------------FOOTER2
Desde el Footer podremos ver informacion sobre la empresa que en este caso es Lorem Ipsum y si el usuario desea mas información al pulsar Read More lo redirigiremos a Know Us por otra parte también tenemos los botones para llevarnos a las redes sociales en este caso los botones no tienen ninguna accion asociada.
También podremos encontrar la direccion de nuestra sede y si queremos mandar una consulta, nos redigira a Contact
----------------------------------

