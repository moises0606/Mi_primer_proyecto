-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entradas_8_dummies`
--

DROP TABLE IF EXISTS `entradas_8_dummies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entradas_8_dummies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dni` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `date_birthday` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fllegada` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fsalida` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `tmotivo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `motivo` text COLLATE utf8_spanish_ci,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entradas_8_dummies`
--

LOCK TABLES `entradas_8_dummies` WRITE;
/*!40000 ALTER TABLE `entradas_8_dummies` DISABLE KEYS */;
INSERT INTO `entradas_8_dummies` VALUES (1,'moises','muriana','muriana@gmail.com','48606014E','622773987','Hombre','10/01/1999','Spain','20/12/2018','22/12/2018','Lo mejor','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento\r\n','10:24:58','2018-01-04'),(2,'Juan','Garcia','juangarc2@gmail.com','34157279W','625879585','Hombre','15/11/1996','Spain','20/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','10:29:27','2018-01-04'),(3,'Pepe','Munoz','pepemu@gmail.com','20993402Z','665874271','Hombre','15/11/1995','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','10:33:32','2018-01-04'),(4,'Manolo','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','10:35:37','2018-01-04'),(14,'Manolo1','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:45:47','2018-01-09'),(15,'Manolo2','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:45:51','2018-01-09'),(16,'Manolo3','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:45:54','2018-01-09'),(17,'Manolo4','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:45:56','2018-01-09'),(18,'Manolo5','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:45:59','2018-01-09'),(19,'Manolo6','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:46:01','2018-01-09'),(20,'Manolo7','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:46:04','2018-01-09'),(21,'Manolo8','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:46:06','2018-01-09'),(22,'Manolo9','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:46:08','2018-01-09'),(23,'Manolo10','Montes','manmont@gmail.com','79669081W','625841588','Hombre','11/06/1997','Spain','19/12/2018','22/12/2018','Gran event','Me gustaria poder acceder a una entrada ya que no me gustaria perderme este gran evento','21:46:11','2018-01-09');
/*!40000 ALTER TABLE `entradas_8_dummies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:43:31
