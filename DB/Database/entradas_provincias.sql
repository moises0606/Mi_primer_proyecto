-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `provincia` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_provincia` (`provincia`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (1,'alava','Alava'),(2,'albacete','Albacete'),(3,'alicante','Alicante'),(4,'almeria','Almeria'),(5,'avila','Avila'),(6,'badajoz','Badajoz'),(7,'illes-balears','Illes Balears'),(8,'barcelona','Barcelona'),(9,'burgos','Burgos'),(10,'caceres','Caceres'),(11,'cadiz','Cadiz'),(12,'castellon','Castellon'),(13,'ciudad-real','Ciudad Real'),(14,'cordoba','Cordoba'),(15,'a-coruna','A Coruna'),(16,'cuenca','Cuenca'),(17,'girona','Girona'),(18,'granada','Granada'),(19,'guadalajara','Guadalajara'),(20,'guipuzcoa','Guipuzcoa'),(21,'huelva','Huelva'),(22,'huesca','Huesca'),(23,'jaen','Jaen'),(24,'leon','Leon'),(25,'lleida','Lleida'),(26,'la-rioja','La Rioja'),(27,'lugo','Lugo'),(28,'madrid','Madrid'),(29,'malaga','Malaga'),(30,'murcia','Murcia'),(31,'navarra','Navarra'),(32,'ourense','Ourense'),(33,'asturias','Asturias'),(34,'palencia','Palencia'),(35,'las-palmas','Las Palmas'),(36,'pontevedra','Pontevedra'),(37,'salamanca','Salamanca'),(38,'santa-cruz-de-tenerife','Santa Cruz de Tenerife'),(39,'cantabria','Cantabria'),(40,'segovia','Segovia'),(41,'sevilla','Sevilla'),(42,'soria','Soria'),(43,'tarragona','Tarragona'),(44,'teruel','Teruel'),(45,'toledo','Toledo'),(46,'valencia','Valencia'),(47,'valladolid','Valladolid'),(48,'vizcaya','Vizcaya'),(49,'zamora','Zamora'),(50,'zaragoza','Zaragoza'),(51,'ceuta','Ceuta'),(52,'melilla','Melilla');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:43:31
