-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entradast`
--

DROP TABLE IF EXISTS `entradast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entradast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `apellidos` varchar(80) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dni` varchar(120) DEFAULT NULL,
  `telefono` varchar(150) DEFAULT NULL,
  `sexo` varchar(100) DEFAULT NULL,
  `date_birthday` varchar(100) DEFAULT NULL,
  `fllegada` varchar(100) DEFAULT NULL,
  `fsalida` varchar(100) DEFAULT NULL,
  `tmotivo` varchar(200) DEFAULT NULL,
  `motivo` text,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entradast`
--

LOCK TABLES `entradast` WRITE;
/*!40000 ALTER TABLE `entradast` DISABLE KEYS */;
INSERT INTO `entradast` VALUES (5,'assssssssssds','ssssssssssssdf','SSSSS@GMAIL.COM','48606014E','622773987','Hombre','12/10/2017','10/12/2017','10/12/2017','ssssssss','sssssssssssssssssssssssssssssssvvvvvvvvvvvvvvvvvvvv','12:05:33','2017-12-10'),(6,'ssssssss','ssssss','SSSSS@GMAIL.COM','48606014E','622773987','Hombre','12/10/2017','10/12/2017','10/12/2017','sssssssssss','lplplplplpldlplpdlpelcplaecplasssss','12:17:53','2017-12-10'),(7,'ssssssss','ssssss','SSSSS@GMAIL.COM','48606014E','622773987','Hombre','12/10/2017','10/12/2017','10/12/2017','sssssssssss','lplplplplpldlplpdlpelcplaecplasssss','12:46:29','2017-12-10');
/*!40000 ALTER TABLE `entradast` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:43:31
