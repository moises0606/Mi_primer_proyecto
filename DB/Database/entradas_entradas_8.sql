-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entradas_8`
--

DROP TABLE IF EXISTS `entradas_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entradas_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dni` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `date_birthday` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fllegada` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fsalida` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `tmotivo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `motivo` text COLLATE utf8_spanish_ci,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entradas_8`
--

LOCK TABLES `entradas_8` WRITE;
/*!40000 ALTER TABLE `entradas_8` DISABLE KEYS */;
INSERT INTO `entradas_8` VALUES (9,'moises','muriana','muriana@gmail.com','48606014E','622773987','Hombre','10/01/1999','Spain','20/12/2017','22/12/2017','sssss','ssssssss','20:50:40','2017-12-13'),(14,'cesar','muriana','muriana@gmail.com','48606013K','622773987','Hombre','10/01/1999','Spain','20/12/2017','22/12/2017','sssss','ssssssss','20:58:18','2017-12-14'),(21,'isabel','barber','msa@gmail.com','58742487R','625874125','Mujer','19/05/1999','Francia','19/12/2017','19/12/2017','aaaaa','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','19:46:32','2017-12-16'),(22,'isabel','barber','msjf@gmail.com','21589632F','695235698','Mujer','19/05/1999','Francia','20/12/2017','25/12/2017','aaaaaaaaa','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','19:49:44','2017-12-16'),(23,'vero','skanfiuel','kdbv@gmail.com','65897554T','632589541','Mujer','16/12/1982','Portugal','16/12/2017','16/12/2017','ssssssss','ssssssssssssssssssssssssssssssssssssss','19:51:25','2017-12-16'),(24,'isabel','barber','isabel','75895421R','632587412','Mujer','16/12/2017','Francia','16/12/2017','16/12/2017','ssssss','ssssssssssssssssssssssssssssssssss','21:14:28','2017-12-16'),(25,'isabel','barber','lsjfeby@gmail.com','48569521K','624578975','Mujer','16/12/2017','Francia','16/12/2017','16/12/2017','sssssss','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','21:15:31','2017-12-16'),(28,'josesp','jsaobf','sssss@gmail.com','48606014O','625874125','Hombre','20/12/2017','Espana','20/12/2017','20/12/2017','sssssss','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','21:52:01','2017-12-20'),(29,'moises','muriana','sssss@gmail.com','48606014E','625874125','Hombre','20/12/2017','Espana','20/12/2017','20/12/2017','aaaaaaa','qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq','21:52:28','2017-12-20'),(30,'yusep','sgteww','sssss@gmail.com','48606014Q','622778587','Hombre','21/12/2017','Espana','21/12/2017','21/12/2017','aaaaaaa','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','18:41:49','2017-12-21'),(34,'cesar','muriana','sssss@gmail.com','45314947W','625874127','Hombre','10-03-1999','Espana','15-03-2018','16-03-2018','wwwww','wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww','16:52:43','2018-03-08'),(35,'moises','muriana','sssss@gmail.com','37174688H','622587425','Hombre','09-03-1999','Espana','08-03-2018','09-03-2018','wwwwwww','wwwwwwwwwwwwwwwwwwwwwwwwwwwww','19:00:23','2018-03-08');
/*!40000 ALTER TABLE `entradas_8` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:43:31
