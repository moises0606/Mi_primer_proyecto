<div id="contenido">
    <div class="container">

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karma">

        <style>
        body,h1,h2,h3,h4,h5,h6 {font-family: "Karma", sans-serif}
        .w3-bar-block .w3-bar-item {padding:20px}
        </style>
        
        <div class="row">
                <h3><?php echo $idioma['dummies1'] ?></h3>
        </div>

        <body>
        <?php
            if ($rdo->num_rows === 0){
                echo '<tr>';
                echo '<td align="center"  colspan="3">NO HAY NINGUNA ENTRADA</td>';
                echo '</tr>';
            }else{
            echo '<div class="w3-row-padding w3-padding-16 w3-center" id="food">';

                foreach ($rdo as $row) {

                        echo '<div class="w3-quarter">';
                            echo '<h3>'. $row['nombre'] .'</h3>';
                            echo '<h3>'. $row['tmotivo'] .'</h3>';
                            echo '<p>'. $row['motivo'] . '</p>';
                        echo '</div>';
                }
            echo '</div>';
            }
        ?>
        </body>    	
    </div>
</div>