$(document).ready(function () {
var cart = [];
showCart();
    $("#btnAdd").click(function() {
        addToCart();
        console.log(localStorage);
    });

    $(document).on('click', "#sumar", function() {
        var value = this.getAttribute('value');
        console.log(value);
        addToCart2(value, 1);
    });

    $(document).on('click', "#eliminar", function() {
        var value = this.getAttribute('value');
        addToCart2(value, 0);
    });

    $(document).on('click', ".eliminarbut", function() {
        var id = this.getAttribute('id');
        console.log(id);
        deleteItem(id);
        document.getElementById(id).style.visibility = "hidden";

    });

    $(document).on('click', "#checkout", function() {
        var total=0;
        $.each(cart, function (i, valor) {
            console.log(valor);
            total=(valor.cant*valor.Precio);
            
            var entrada = { entrada : valor.Producto , cant: valor.cant , ptotal: total  };

            $.post("module/cart/controller/controller_cart.php?op=checkout", entrada, function(error) {
                var json = JSON.parse(error);
                console.log(error);
                console.log(json);

                if (json == "1"){
                    alert("Tienes que estar loggeado");
                    $callback = "index.php?page=controller_login&op=list_login";
                    window.location.href=$callback;
                    return
                }else if (json == "true"){
                    alert("La compra se ha realizado con exito");
                    deleteItem(valor.Producto);
                }
            });
        });
        $("#compra").css("visibility", "hidden");
    });

    $(function () {
        if (localStorage.cart){
            cart = JSON.parse(localStorage.cart);
            showCart();
        }
    });

    function addToCart() {
        //var precio = document.getElementById('precio').value;
        var precio=10;
        var nombre = document.getElementById("nombrelistev").innerHTML;
      
        for (var i in cart) {
            if(cart[i].Producto == nombre){
                var cant = cart[i].cant;
                cant++;
                cart[i].cant = cant;
                showCart();
                saveCart();
                return;
            }
        }
        cant = 1;

        var item = { Producto: nombre,  Precio: precio, cant: cant };
        cart.push(item);
        saveCart();
        showCart();
    }

    function addToCart2(nombre, type) {
        for (var i in cart) {
            if(cart[i].Producto == nombre){
                var cant = cart[i].cant;
                if (type==1)
                    cant++;
                if (type==0)
                    cant--;
                if (cant<=0) {
                    deletebuton(nombre);
                    return
                }
                cart[i].cant = cant;
                showCart();
                saveCart();
                return;
            }
        }
    }

    function deletebuton(index){
        document.getElementById(index).style.visibility = "visible";
    }

    function deleteItem(index){
        cart.splice(index,1);
        showCart();
        saveCart();
    }

    function saveCart() {
        if ( window.localStorage){
            localStorage.cart = JSON.stringify(cart);
        }
    }

    function showCart() {
        //console.log(cart.length);
        if (cart.length == 0) {
            $("#cesta").empty();
            $("#cesta").css("visibility", "visible");
                        var row =    "<div id=cartbody2>"+
                            "<h2>Tu cesta esta vacia</h2>" +
                            "<div><a href=index.php?page=controller_events&op=list><button id=cartempty>Comprar</button></a></div>" +                         
                            "<p>Tu cesta esta vacia, si deseas comprar alguna entrada puedes pulsar en el boton para poder ver los proximos eventos disponible, ahí podrás elegir la entrada del evento al que desees ir.</p>"
                         "</div>";
            $("#cesta").append(row);
            return;
        }

        $("#cesta").css("visibility", "visible");
        $("#compra").css("visibility", "visible");

        $("#cesta").empty();
        $("#precio").empty();

        var total=0;
        for (var i in cart) {
            var item = cart[i];
            var row =    "<div id=cartbody>"+
                             "<div id=linea></div>" +
                             "<div id=cardata>"+
                                 "<div id=nomprod>" + item.Producto + "</div>" +
                                 "<div id=precioc>Precio " +item.Precio + "€</div>" +
                                 "<div id=cantidad>" + item.cant + " entradas</div>" + 
                                 "<div id=total>" +item.cant * item.Precio + "€</div>"+
                                 "</div>"+
                             "<div><a><img class=cartbutt id=sumar value="+item.Producto+" src=view/img/sumar.png></img></a></div>" +         
                             "<div><a><img class=cartbutt id=eliminar value="+item.Producto+" src=view/img/eliminar.png></img></a></div>" +
                             "<div><button class=eliminarbut id="+item.Producto+">Eliminar del carrito</button></div>" +                         
                         "</div>";
            total=total+(item.cant * item.Precio);
            //console.log(total);
            //console.log(item.cant);
            //console.log(item.Precio);
            $("#cesta").append(row);
        }
            var total="<div id=precios>"+total+"€</div>";
            $("#precio").append(total);
    }
});              
