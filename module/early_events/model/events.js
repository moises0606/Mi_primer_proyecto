var eventos;
var deventos2;
var eventsapi;

var loadtipoeventos = function () {
    $("#tipo_eventos").empty();
    $.each(eventos, function (i, valor) {
        $("#tipo_eventos").append("<option value='" + i + "'>" + valor + "</option>");
    });
};

var loadeventos = function () {
    var tipo_event = document.getElementById('tipo_eventos').value;
    
    if(tipo_event >= 0){
        var datos = { tipo_evento : tipo_event  };

        //console.log(datos);
        //console.log(eventos);

        for (i=0; i<=tipo_event; i++) {
            if (i==datos.tipo_evento)
                var tipo = { tipo_evento : eventos[i]  };
        }

        //console.log(tipo);
        
        $.post("module/early_events/controller/controller_events.php?op=eventos", tipo, function(deventos) {
            //console.log(deventos);
            deventos2 = JSON.parse(deventos);

        var $comboEventos = $("#eventos");
            $comboEventos.empty();

            //console.log(deventos2);
            delete deventos2['events'];

            $.each(deventos2, function(index, evento) {
                $comboEventos.append("<option>" + evento + "</option>");
            });
            listar_evento();
        });
    }else{
        var $comboEventos = $("#eventos");
        $comboEventos.empty();
        $comboEventos.append("<option>Primero seleccione un tipo de evento</option>");
    }
};

var listar_evento = function () {
    $("#listar").empty();
    var provincia = document.getElementById('provincias').value;
    //console.log(provincia.length);
    if (provincia.length>3) {
    var provincia= { provincia : document.getElementById('provincias').value  };
    var evento=document.getElementById('tipo_eventos').value;
        for (i=0; i<=evento; i++) {
            if (i==evento)
                var evento = { tipo_evento : eventos[i]  };
        }
    var actividad={ evento : document.getElementById('eventos').value  };

    //console.log(provincia);
    //console.log(evento);
    //console.log(actividad);
    var datos={datos : [provincia, evento, actividad]};
    //console.log(datos);

    $.post("module/early_events/controller/controller_events.php?op=listar_evento",datos, function(deventos) {
            //console.log(deventos);
            var json = JSON.parse(deventos);
            //console.log(json[0].provincia);
           //console.log(json[0].evento);
            //console.log(json[1]);

        $("#listar_evento").show();
        $("#nombrelistev").html(json[0].evento);
        $("#descripcionlistev").html("en " + json[0].provincia);
        $("#provev").html("Entrada de " + json[1]+ " de");
       
        });
    }
};

var listar_evento_api = function () {

    var provincia = document.getElementById('provincias').value;
    if (provincia.length>3) {
    $("#eventosapi").empty();
    var $divapiev = $("#eventosapi");

        $.ajax({
        type : 'GET',
        url  : 'https://app.ticketmaster.com/discovery/v2/events.json?apikey=9qSmw4LE7hMbVAGEobA08l1TKU2hTACy&city='+provincia,
        async:true,
        dataType: "json",
            success: function(json) {
                $.each(json, function (i, valor) {
                    if (valor.events){
                        $divapiev.append('<div><h4 id="earlyh4">' + "Eventos que te pueden interesar:" + '</h4></div>');
                        eventsapi = valor.events;
                        //console.log(eventsapi);
                        for (i=0; i<eventsapi.length; i++){
                            //console.log(eventsapi[i].name);
                        $divapiev.append('<div id="bootstrev"><p>' + eventsapi[i].name + '</p>  <button id="btnrsv" onclick="addToCart()" >Reserve</button></div>');

                        }
                    }
                });
            },
        });
    }
};

//------------------------------------------------------------
$(document).ready(function () {
$.get("module/early_events/controller/controller_events.php?op=tipo_eventos", function (data) {
        //console.log(data);
        eventos = JSON.parse(data);
        //console.log(eventos);
        loadtipoeventos();
        loadeventos();
        //listar_evento();
});

    $("#tipo_eventos").change(function() {
        loadeventos();
        //setTimeout("listar_evento()",15);
    });

    $("#eventos").change(function() {
        listar_evento();
    });

    $('#provincias').keyup(function(){
        var prov = document.getElementById('provincias').value;

        $.get("module/early_events/controller/controller_events.php?op=provincias", function (data) {    
            var json = JSON.parse(data);
            //console.log(data);
            //console.log(json);

            var prov=[];
            $.each(json, function (i, valor) {
                prov.push(valor);
            });

            //console.log(prov);

            $('#provincias').autocomplete({
                source: prov
            });
            listar_evento();
            listar_evento_api();
        });
    });  

    $(document).on('click', "#like", function() {
        var nombre = document.getElementById("nombrelistev").innerHTML;
        var nombre1 = document.getElementById("provev").innerHTML;
        var nombre2 = document.getElementById("descripcionlistev").innerHTML;

        //console.log(nombre);
        //console.log(nombre1);
        //console.log(nombre2);

        producto= nombre1 + " " + nombre + " "+ nombre2;
        $.ajax({
                type : 'POST',
                url  : "module/early_events/controller/controller_events.php?op=like",
                data: {producto},
                    success: function(json) {
                        console.log(json);
                        var data = JSON.parse(json);
                        console.log(data);
                        
                        if (data == "true" ){
                            alert("Se ha añadido a me gusta");
                        }

                        if (data == 1 ){
                            alert("Necesitas estar logeado");
                            window.location.href="index.php?page=controller_login&op=list_login";
                        }
                    },
        });
    });
});              
