<div id="recoverbody">
    <h2>Recover Password</h2>

    <form method="post" autocomplete="on" name="recover_pass" id="recover_pass">
	<table border='0'>
	    <tr>
			<td>			
                <h5><?php echo $idioma['login4'] ?></h5>
				<input type="text" id="recemail" name="recemail" placeholder="example@gmail.com" value=""/>
			</td>
		</tr>
		<tr>
			<td><font color="red">
                    <span id="rec_email" class="error">

                    </span>
            </font></font></td>
		</tr>
		<div>
		<tr class="trpassw">
			<td>
				<h5><?php echo $idioma['login5'] ?></h5>
				<input type="password" id="passw2" name="passw2" placeholder="*****" value=""/>
			</td>
		</tr>
		<tr>
		<tr>
			<td><font color="red">
                    <span id="rec_passw2" class="error">

                    </span>
            </font></font></td>
		</tr>
		<tr class="trpassw">
			<td>
				<h5><?php echo $idioma['login6'] ?></h5>
				<input type="password" id="passw3" name="passw3" placeholder="*****" value=""/>
			</td>
		</tr>
		<tr>
			<td><font color="red">
                    <span id="rec_passw3" class="error">

                    </span>
            </font></font></td>
		</tr>
	</table>
	<input type="hidden" name="recoveraux"/>

    <input name="recover" class="Button_gray" id="recover" type="button" value="Recover";/>
    <a class="Button_gray" href="index.php?page=controller_login&op=list_login">Volver</a>
    </form>
</div>
