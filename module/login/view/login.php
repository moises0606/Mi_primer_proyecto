<div id="login">
    <h2>Login</h2>

    <form method="post" autocomplete="on" name="formlogin" id="formlogin">
	<table border='0'>
	    <tr>
			<td>
                <?php
                        if(!isset($_POST['usuario'])){
                        	$_POST['usuario']="";
                    }
                ?>				
                <h5><?php echo $idioma['login1'] ?></h5>
				<input type="text" id="usuario" name="usuario" placeholder="usuario" value="<?php echo $_POST['usuario'] ?>"/>
			</td>
		</tr>
		<tr>
			<td><font color="red">
                    <span id="e_usuario" class="error">

                    </span>
            </font></font></td>
		</tr>
		<tr>
			<td>
				<h5><?php echo $idioma['login2'] ?></h5>
				<input type="password" id="passw" name="passw" placeholder="*****" value=""/>
			</td>
		</tr>
		<tr>
			<td><font color="red">
                    <span id="e_passw" class="error">

                    </span>
            </font></font></td>
		</tr>
	</table>
	<input type="hidden" name="loginaux"/>

    <input name="loginb" class="Button_gray_login" id="loginb" type="button" value="Login";/>
    <a class="Button_gray_login" href="index.php?page=controller_login&op=register">Registrarse</a>  
    <a class="Button_gray_login" href="index.php?page=homepage">Volver</a>
    <a id="lrecover" class="Button_gray_login" href="index.php?page=controller_login&op=list_recover">Recover Pass</a> 
    </form>
</div>
