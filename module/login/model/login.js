var username;
var email;
var passw;

var username1;
var passw1;

function validate_login() {
    //console.log(document.formlogin.usuario.value);

    if (document.formlogin.usuario.value.length===0){
        document.getElementById('e_usuario').innerHTML = "Tiene que escribir su usuario";
        document.formlogin.usuario.focus();
        return 0;
    }
    document.getElementById('e_usuario').innerHTML = "";

    if (document.formlogin.passw.value.length===0){
        document.getElementById('e_passw').innerHTML = "Tiene que escribir su contraseña";
        document.formlogin.passw.focus();
        return 0;
    }
    document.getElementById('e_passw').innerHTML = "";

    //document.formlogin.submit();
    //document.formlogin.action="index.php?page=controller_login&op=list_login";
}

function validate_register_login() {
    //console.log(document.register_user.usuario.value);

    if (document.register_user.rusuario.value.length===0){
        document.getElementById('re_usuario').innerHTML = "Tiene que escribir su usuario";
        document.register_user.rusuario.focus();
        return 0;
    }
    document.getElementById('re_usuario').innerHTML = "";
    
    if (document.register_user.remail.value.length===0){
        document.getElementById('re_email').innerHTML = "Tiene que escribir su email";
        document.register_user.remail.focus();
        return 0;
    }
    document.getElementById('re_email').innerHTML = "";

    if (document.register_user.passw1.value.length===0){
        document.getElementById('e_passw1').innerHTML = "Tiene que escribir su contraseña";
        document.register_user.passw1.focus();
        return 0;
    }
    document.getElementById('e_passw1').innerHTML = "";

    //document.register_user.submit();
    //document.register_user.action="index.php?page=controller_login&op=register";
}

function validate_recover_login() {
    //console.log(document.register_user.usuario.value);

    if (document.recover_pass.recemail.value.length===0){
        document.getElementById('rec_email').innerHTML = "Tiene que escribir su email";
        document.recover_pass.recemail.focus();
        return 0;
    }
    document.getElementById('rec_email').innerHTML = "";
    
    if (document.recover_pass.passw2.value.length===0){
        document.getElementById('rec_passw2').innerHTML = "Tiene que escribir su contraseña";
        document.recover_pass.passw2.focus();
        return 0;
    }
    document.getElementById('rec_passw2').innerHTML = "";

    if (document.recover_pass.passw3.value.length===0){
        document.getElementById('rec_passw3').innerHTML = "Tiene que repetir su contraseña";
        document.recover_pass.passw3.focus();
        return 0;
    }
    document.getElementById('rec_passw3').innerHTML = "";

    //document.register_user.submit();
    //document.register_user.action="index.php?page=controller_login&op=register";
}

var register = function () {
    if ((username.length > 0)&&(email.length > 0)&&(passw.length > 0)){
        var datos = { usuario : username, email : email, passw : passw };

        //console.log(datos);

        $.post("module/login/controller/controller_login.php?op=register_db", datos, function(data) {
            //console.log(deventos);
            var data = JSON.parse(data);

            if (data['usuario'] != null)
                document.getElementById('re_usuario').innerHTML = "El usuario ya existe";
            else
                document.getElementById('re_usuario').innerHTML = "";

            if (data['email'] != null)
                document.getElementById('re_email').innerHTML = "El email ya existe";
            else
                document.getElementById('re_email').innerHTML = "";

            if (data['callback'] != null)
                window.location.href="'.data['callback'].'";

            if (data['succes'] != null){
                alert("Registrado en la base de datos correctamente");
                window.location.href="index.php?page=controller_login&op=list_login";
            }
        });
    }
}

var login1 = function () {
    if ((username1.length > 0)&&(passw1.length > 0)){
        var datos = { usuario : username1, passw : passw1 };

        $.post("module/login/controller/controller_login.php?op=login", datos, function(data) {
            console.log(data);
            var data = JSON.parse(data);
            //console.log(data);
            if (data['usuario'] != null)
                document.getElementById('e_usuario').innerHTML = data['usuario'];
            else
                document.getElementById('e_usuario').innerHTML = "";

            if (data['passw'] != null)
                document.getElementById('e_passw').innerHTML = data['passw'];
            else
                document.getElementById('e_passw').innerHTML = "";

            if (data['callback'] != null)
                window.location.href="'.data['callback'].'";

            if (data['succes'] != null){
                alert('Acceso permitido');
                window.location.href="index.php?page=homepage";
            }
        });
    }
}

var recover_pass = function (email1, passw2, passw3) {
    //console.log(email1);
    //console.log(passw2);
    //console.log(passw3);

    if ((email1.length > 0)&&(passw2.length > 0)&&(passw3.length > 0)){
        var datos = { email : email1, passw : passw2, passw1 : passw3 };

        if (passw2 == passw3) {
            $.ajax({
                type : 'POST',
                url  : "module/login/controller/controller_login.php?op=recover",
                data: {email1, passw2},
                    success: function(json) {
                        console.log(json);
                        var data = JSON.parse(json);
                        if (data['success1']){
                                alert(data['success1']);
                                window.location.href="index.php?page=controller_login&op=list_login";
                        }
                    },
            });
        } else {
            document.getElementById('rec_passw2').innerHTML = "Las contraseñas no coinciden";
            document.getElementById('rec_passw3').innerHTML = "Las contraseñas no coinciden";

        }
    }
}

var isthereemail = function (email1) {

    if ((email1.length > 0)){
        //console.log(email1);
        $.ajax({
            type : 'POST',
            url  : "module/login/controller/controller_login.php?op=recover",
            data: {email1},
                success: function(json) {
                    console.log(json);
                    var data = JSON.parse(json);
                    console.log(json);
                    if (data['success']){
                        $(".trpassw").css("visibility", "visible");
                        document.getElementById('rec_email').innerHTML = "";
                    }
                    if (data['email'])
                        document.getElementById('rec_email').innerHTML = "El email no existe";
                    //console.log(json);
                },
        });
    }
}

$(document).ready(function () {
    //JS del REGISTER
    $("#registerb").click(function() {
        validate_register_login();
        
        username = document.getElementById('rusuario').value;
        email = document.getElementById('remail').value;
        passw = document.getElementById('passw1').value;

        register();
        });

    $("#passw1").keypress(function(e) {
        if(e.which == 13) {
            validate_register_login();
            
            username = document.getElementById('rusuario').value;
            email = document.getElementById('remail').value;
            passw = document.getElementById('passw1').value;

            register();
        }
    });

    //JS del LOGIN
    $("#loginb").click(function() {
            validate_login();

            username1 = document.getElementById('usuario').value;
            passw1 = document.getElementById('passw').value;

            login1();
    });

    $("#passw").keypress(function(e) {
        if(e.which == 13) {
            validate_login();

            username1 = document.getElementById('usuario').value;
            passw1 = document.getElementById('passw').value;

            login1();
        }
    });

    //JS del RECOVER
    $("#recover").click(function() {
            validate_recover_login();

            email1 = document.getElementById('recemail').value;
            passw2 = document.getElementById('passw2').value;
            passw3 = document.getElementById('passw3').value;

            recover_pass(email1, passw2, passw3);
    });

    $("#passw3").keypress(function(e) {
        if(e.which == 13) {
            validate_recover_login();

            email1 = document.getElementById('recemail').value;
            passw2 = document.getElementById('passw2').value;
            passw3 = document.getElementById('passw3').value;

            recover_pass(email1, passw2, passw3);
        }
    });
    
    $("#recemail").keypress(function() {
        email1 = document.getElementById('recemail').value;
        //console.log(email1);
        isthereemail(email1);
    });
});
