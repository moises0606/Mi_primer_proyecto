<?php

    $path = $_SERVER['DOCUMENT_ROOT']. '/programacio/workspace_moises/8_MVC_CRUD/';
    include($path . 'module/login/model/DAOlogin.php');
    
    if (!isset($_SESSION))
        session_start();

    switch($_GET['op']){

        case 'list_login':
                include("module/login/view/login.php");
            break;

        case 'login':
            try{
                $daouser = new DAOlogin();
                $user = $daouser->isthere($_POST['usuario'], 'username');
                $passw = $daouser->isthere2($_POST['passw'], 'password', $_POST['usuario']);
                $type = $daouser->thistype($_POST['usuario']);

                if (($user)&&($passw)){
                    $user1=get_object_vars($user);
                    $type1=get_object_vars($type);

                    $_SESSION['user']=$user1['username'];
                    $_SESSION['type']=$type1['type'];
                    $_POST="0";
                    $error['succes']="Acceso permitido";
                }else{
                    $error="";
                    if (!$user)
                        $error['usuario']="El usuario no existe";
                    else{
                        if (!$passw)
                            $error['passw']="La contrasena es incorrecta";
                    }
                }
            }catch (Exception $e){
                $error['callback'] = 'index.php?page=503';
                echo json_encode($error);
                die();
            }

            echo json_encode($error);

        exit;
        break;

        case 'register':
                include("module/login/view/register.php");
            break;

        case 'register_db':
            try{
                $daouser = new DAOlogin();
                $user = $daouser->isthere($_POST['usuario'], 'username');
                $email = $daouser->isthere($_POST['email'], 'email');

                if ((!$user)&&(!$email)){
                    $rdo = $daouser->insert_user($_POST);
                    $_POST="0";
                    $error['succes']="El registro ha sido un exito";
                }else{
                    $error="";
                    if ($user)
                        $error['usuario']="El usuario ya existe";
                    if ($email)
                        $error['email']="El email ya existe";
                }
            }catch (Exception $e){
                $error['callback'] = 'index.php?page=503';
                echo json_encode($error);
                die();
            }

            echo json_encode($error);

        exit;
        break;

        case 'list_recover':
                include("module/login/view/recover.php");
            break;

        case 'recover':

            try{
                $daouser = new DAOlogin();
                $email = $daouser->istheremail($_POST['email1']);
                if (isset($_POST['passw2'])){
                    $rdo = $daouser->updatepass($_POST['email1'], $_POST['passw2']);
                    $passw = $daouser->istheremail2($_POST['email1'], $_POST['passw2']);
                }else
                    $passw="";
                if (($email)&&($passw)){
                    $error['success1']="Se ha cambiado la contrasenya";
                }else if ($email){
                    $error['success']="El email existe";
                }else{
                    $error="";
                    if (!$email)
                        $error['email']="El email es incorrecto";
                    if (!$passw)
                        $error['passw']="La passw no se ha actualizado";
                }
            }catch (Exception $e){
                $error['callback'] = 'index.php?page=503';
                echo json_encode($error);
                die();
            }
            $_POST="";
            echo json_encode($error);

        exit;
        break;

        case 'logout':
                session_start();
                session_unset();
                session_destroy();

                $callback = 'index.php';
                die('<script>window.location.href="'.$callback .'";</script>');
            break;

        /*
        A partir de ahora podremos observar el codigo 
        del login y del register, este codigo fue programado 
        en su gran mayoria en php (es una primera version la cual mantengo guardada)
        */
        case 'list_login_php':
            if (isset($_POST['loginaux'])){
                $rdo=' ';
                    //$_SESSION['register']=$_POST;
                    try{
                        $daouser = new DAOlogin();
                        $user = $daouser->isthere($_POST['usuario'], 'username');
                        $passw = $daouser->isthere2($_POST['passw'], 'password', $_POST['usuario']);
                        $type = $daouser->thistype($_POST['usuario']);
                        $user1=get_object_vars($user);
                        $type1=get_object_vars($type);

                        if (isset($_POST['loginaux'])&&($user1)&&($passw)){
                            $_SESSION['user']=$user1['username'];
                            $_SESSION['type']=$type1['type'];
                            $_POST="0";

                        }else{
                            $error="";
                            if (!$user)
                                $error['usuario']="El usuario no existe";
                            else{
                                if (!$passw)
                                    $error['passw']="La contraseña es incorrecta";
                            }
                        }
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }

                   if(($user)&&($passw)){
                        $callback = 'index.php';
                        echo '<script language="javascript">alert("Acceso permitido")</script>';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }elseif ((!$user1)||(!$passw)){
                        echo '<script language="javascript">alert("Acceso denegado")</script>';
                    }else {
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
                }
                include("module/login/view/login.php");
            break;

        case 'register_php':
            //print_r($_POST);
            if (isset($_POST['register'])){
                $rdo=' ';
                    //$_SESSION['register']=$_POST;
                    try{
                        $daouser = new DAOlogin();
                        $user = $daouser->isthere($_POST['usuario'], 'username');
                        $email = $daouser->isthere($_POST['email'], 'email');

                        if (isset($_POST['register'])&&(!$user)&&(!$email)){
                            $rdo = $daouser->insert_user($_POST);
                            $_POST="0";
                        }else{
                            $error="";
                            if ($user)
                                $error['usuario']="El usuario ya existe";
                            if ($email)
                                $error['email']="El email ya existe";
                        }
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }

                    if(($rdo)&&(!$user)&&(!$email)){
                        $callback = 'index.php?page=controller_login&op=list_login';
                        echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }elseif (($user)||($email)){
                        echo '<script language="javascript">alert("Error revisa tus datos")</script>';
                    }else {
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
            }
                include("module/login/view/register.php");
            break;
        default;
            include("view/inc/error404.php");
            break;
}