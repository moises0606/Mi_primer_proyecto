<div id="contenido">
    <div class="container">
        <div class="jumbotron jumbotron">
          <div class="container">
            <h3><?php echo $idioma['body1'] ?></h3>
            <p class="lead">Aquí podremos ver todas las entradas que hemos introducido en la BBDD</p>
          </div>
        </div>
        
        <div id="imgadd">
                <p><a href="index.php?page=controller_entrada&op=create"><img src="view/img/anadir.png"></a></p>
        </div>
    	

        <div class="row">
<?php
            if (!isset($_SESSION['op']))
                $_SESSION['op']="";
            
            if ($_SESSION['op']==="read"){
                $_SESSION['op']="";
                $entrada=$_SESSION['entrada'];
                include("module/entrada/view/read_entrada.php");
                
            }

?>
    		<table id="tablelist">
                <tr>
                    <td width=125><b>Id</b></th>
                    <td width=125><b><?php echo $idioma['body2'] ?></b></th>
                    <td width=125><b><?php echo $idioma['body3'] ?></b></th>
                    <th width=350><b><?php echo $idioma['body4'] ?></b></th>
                </tr>
                    <style type="text/css">/* Ho coloquem açi per a que el css del modal no ens meneje la tabla */
                        td {
                            padding-bottom: 5px;
                        }
                    </style>  
                <?php
                    if(!isset($_SESSION['op']))
                        $_SESSION['op']="";

                    if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NO HAY NINGUN USUARIO</td>';
                        echo '</tr>';
                    }else{
                        
                        foreach ($rdo as $row) {
                            echo '<div class="list">';
                            echo '<p>';
                            echo '<tr>';
                    	   	echo '<td width=125>'. $row['id'] . '</td>';
                    	   	echo '<td width=125>'. $row['dni'] . '</td>';
                    	   	echo '<td width=125>'. $row['nombre'] . '</td>';
                    	   	echo '<td width=350>';
                    	   	print ("<div class='Button_blue1' id='".$row['id']."'>Read</div>");  //READ
                            //echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                            //echo '<a class="Button_blue" href="index.php?page=controller_entrada&op=read&id='.$row['id'].'">Read</a>';
                    	   	echo '&nbsp;';
                    	   	echo '<a class="Button_green" href="index.php?page=controller_entrada&op=update&id='.$row['id'].'">Update</a>';
                    	   	echo '&nbsp;';
                    	   	echo '<a class="Button_red" href="index.php?page=controller_entrada&op=delete&id='.$row['id'].'">Delete</a>';
                    	   	echo '</td>';
                    	   	echo '</tr>';
                            echo '<p>';
                            echo '</div>';
                     

                        }
                    }
                ?>
            </table>
    	</div>
    </div>
</div>

<section id="entrada_modal">
    <div id="modal" style="display: none;">
        <div id="details">
            <div id="container">
                Nombre: <div id="nombre"></div></br>
                Apellidos: <div id="apellidos"></div></br>
                Email: <div id="email"></div></br>
                Dni: <div id="dni"></div></br>
                Telefono: <div id="telefono"></div></br>
                Sexo: <div id="sexo"></div></br>
                Fecha de nacimiento: <div id="date_birthday"></div></br>
                Pais: <div id="country"></div></br>
                Fecha llegada: <div id="fllegada"></div></br>
                Fecha salida: <div id="fsalida"></div></br>
                Nombre motivo: <div id="tmotivo"></div></br>
                Motivo: <div id="motivo"></div></br>
            </div>
        </div>
    </div>
</section>


