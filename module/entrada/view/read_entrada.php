<!DOCTYPE html>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Button to trigger modal -->
<!--<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal">
    Read
</button>-->
    <script>
      $(document).ready(function()
      {
         $("#modal").modal("show");
      });
    </script>


<!-- Modal -->
<!--<?php 'echo <div class="modal fade" id="modal" role="dialog">'; ?>-->

<div class="modal fade" id="modal" role="dialog">;
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            <h2><?php echo $idioma['read1'] ?></h2>
            <h3><?php echo $entrada['nombre'];?> <?php echo $entrada['apellidos'];?></h3>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
             <div id="contenido">
                    <p>
                    <table border='2'>
                        <tr>
                            <td><?php echo $idioma['read2'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['nombre'];
                                ?>
                            </td>
                        </tr>
                    
                        <tr>
                            <td><?php echo $idioma['read3'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['apellidos'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Email: </td>
                            <td>
                                <?php
                                    echo $entrada['email'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><?php echo $idioma['read4'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['dni'];
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $idioma['read5'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['telefono'];
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $idioma['read6'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['sexo'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><?php echo $idioma['read7'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['date_birthday'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><?php echo $idioma['read8'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['country'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><?php echo $idioma['read9'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['fllegada'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><?php echo $idioma['read10'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['fllegada'];
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $idioma['read11'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['tmotivo'];
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><?php echo $idioma['read12'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['motivo'];
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $idioma['read13'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['hora'];
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $idioma['read14'] ?></td>
                            <td>
                                <?php
                                    echo $entrada['fecha'];
                                ?>
                            </td>
                        </tr>
                    </table>
                    </p>
                </div>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <a type="button" class="btn btn-default" href="index.php?page=controller_entrada&op=list"><?php echo $idioma['read15'] ?></a>
            </div>
        </div>
    </div>
</div>