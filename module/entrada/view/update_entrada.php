<div id="contenido">
    <form method="post" autocomplete="on" name="update" id="update">
        <h1>Modificar entrada</h1>
        <table border='0'>
            <tr>
                <td>Nombre: </td>
                <td><input type="text" id="nombre" name="nombre" placeholder="nombre" value="<?php echo $entrada["nombre"] ?>"/></td>
                <td><font color="red">
                    <span id="e_nombre" class="error">
                    <?php
                            if(isset($error['nombre'])){
                            print_r ($error['nombre']);
                        }
                    ?>
                    </span>
                </font></td>
            </tr>
        
            <tr>
                <td>Apellidos: </td>
                <td><input type="text" id="apellidos" name="apellidos" placeholder="apellidos" value="<?php echo $entrada["apellidos"] ?>"/></td>
                <td><font color="red">
                    <span id="e_apellidos" class="error">
                    <?php
                            if(isset($error['apellidos'])){
                            print_r ($error['apellidos']);
                        }
                    ?>
                    </span>
                </font></font></td>
            </tr>
            
            <tr>
                <td>Email: </td>
                <td><input type="text" id="email" name="email" placeholder="ejemplo@gmail.com" value="<?php echo $entrada["email"] ?>"/></td>
                <td><font color="red">
                    <span id="e_email" class="error">
                    <?php
                            if(isset($error['email'])){
                            print_r ($error['email']);
                        }
                    ?>               
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>DNI: </td>
                <td><input type="text" id= "dni" name="dni" placeholder="dni" value="<?php echo $entrada["dni"] ?>" readonly/></td>
                <td><font color="red">
                    <span id="e_dni" class="error">
                    <?php
                            if(isset($error['dni'])){
                            print_r ($error['dni']);
                        }
                    ?>
                    </span>
                </font></font></td>
            </tr>
            
             <tr>
                <td>Telefono: </td>
                <td><input type="text" id= "telefono" name="telefono" placeholder="telefono" value="<?php echo $entrada["telefono"] ?>"/></td>
                <td><font color="red">
                    <span id="e_telefono" class="error">
                    <?php
                            if(isset($error['telefono'])){
                            print_r ($error['telefono']);
                        }
                    ?>       
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Sexo: </td>
                <td>
                    <?php
                        if ($entrada['sexo']==="Hombre"){
                    ?>
                        <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Hombre" checked/>Hombre
                        <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Mujer"/>Mujer
                    <?php    
                        }else{
                    ?>
                        <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Hombre"/>Hombre
                        <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Mujer" checked/>Mujer
                    <?php   
                        }
                    ?>
                </td>
                <td><font color="red">
                    <span id="e_sexo" class="error">

                    </span>
                </font></font></td>
            </tr>
            
            <tr>
                <td><label for="date_birthday">Fecha nacimiento:</label></td>
                <td><input id="date_birthday" type="text" name="date_birthday" placeholder="fecha de nacimiento" readonly="readonly" value="<?php echo $entrada["date_birthday"] ?>"/></td>
                <td><font color="red">
                    <span id="e_date_birthday" class="error">
                    <?php
                            if(isset($error['date_birthday'])){
                            print_r ($error['date_birthday']);
                        }
                    ?>  
                    </span>
                </font></font></td>
            </tr>
            
            <tr>
                <td>Pais: </td>
                <td><select id="country" name="country" placeholder="pais">
                    <?php
                        if($entrada['country']==="Espana"){
                    ?>
                        <option value="Espana" selected>España</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Francia">Francia</option>
                    <?php
                        }elseif($entrada['country']==="Portugal"){
                    ?>
                        <option value="Espana">España</option>
                        <option value="Portugal" selected>Portugal</option>
                        <option value="Francia">Francia</option>
                    <?php
                        }elseif($entrada['country']==="Francia"){
                    ?>
                        <option value="Espana">España</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Francia" selected>Francia</option>
                    <?php
                        }
                    ?>

                    </select></td>
                <td><font color="red">
                    <span id="e_country" class="error">

                    </span>
                </font></font></td>
            </tr>
            
            <tr>
                <td><label for="fllegada">Fecha llegada:</label></td>
                <td><input id="fllegada" type="text" name="fllegada" placeholder="fecha de llegada" readonly="readonly" value="<?php echo $entrada["fllegada"] ?>"/></td>
                <td><font color="red">
                    <span id="e_fllegada" class="error">
                    <?php
                            if(isset($error['fllegada'])){
                            print_r ($error['fllegada']);
                        }
                    ?>  
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td><label for="fsalida">Fecha salida:</label></td>
                <td><input id="fsalida" type="text" name="fsalida" placeholder="fecha de salida" readonly="readonly" value="<?php echo $entrada["fsalida"] ?>"/></td>
                <td><font color="red">
                    <span id="e_fsalida" class="error">
                    <?php
                            if(isset($error['fsalida'])){
                            print_r ($error['fsalida']);
                        }
                    ?>  
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Nombre motivo: </td>
                 <td><input type="text" id="tmotivo" name="tmotivo" placeholder="tmotivo" value="<?php echo $entrada["tmotivo"] ?>"/></td>
                <td><font color="red">
                    <span id="e_tmotivo" class="error">
                    <?php
                            if(isset($error['tmotivo'])){
                            print_r ($error['tmotivo']);
                        }
                    ?>
                    </span>
                </font></font></td>
            </tr>
            
            <tr>
                <td>Motivo: </td>
                <td><textarea cols="30" rows="5" id="motivo" name="motivo" placeholder="motivo"><?php echo $entrada["motivo"] ?></textarea></td>
                <td><font color="red">
                    <span id="e_motivo" class="error">
                    <?php
                            if(isset($error['motivo'])){
                            print_r ($error['motivo']);
                        }
                    ?>
                    </span>
                </font></td>
            </tr>
            <input type="hidden" name="updater"/>

            <tr>
                <td><input type="button" id="updater" class="Button_gray" value="Modificar" onclick="validate_entrada_update()";/></td>
                <td align="right"><a class="Button_gray" href="index.php?page=controller_entrada&op=list">Volver</a></td>
            </tr>
        </table>
    </form>
</div>