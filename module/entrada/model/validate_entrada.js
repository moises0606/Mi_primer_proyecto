function validate_entrada() {
    console.log(document.crea_entrada.nombre.value);

    if (document.crea_entrada.nombre.value.length===0){
        document.getElementById('e_nombre').innerHTML = "Tiene que escribir su nombre";
        document.crea_entrada.nombre.focus();
        return 0;
    }
    document.getElementById('e_nombre').innerHTML = "";
    
    if (document.crea_entrada.apellidos.value.length===0){
        document.getElementById('e_apellidos').innerHTML = "Tiene que escribir sus apellidos";
        document.crea_entrada.apellidos.focus();
        return 0;
    }
    document.getElementById('e_apellidos').innerHTML = "";
    
    if (document.crea_entrada.email.value.length===0){
        document.getElementById('e_email').innerHTML = "Tiene que escribir su email";
        document.crea_entrada.email.focus();
        return 0;
    }
    document.getElementById('e_email').innerHTML = "";

    if (document.crea_entrada.dni.value.length===0){
        document.getElementById('e_dni').innerHTML = "Tiene que escribir su dni";
        document.crea_entrada.dni.focus();
        return 0;
    }
    document.getElementById('e_dni').innerHTML = "";

    if (document.crea_entrada.telefono.value.length===0){
        document.getElementById('e_telefono').innerHTML = "Tiene que escribir su telefono";
        document.crea_entrada.telefono.focus();
        return 0;
    }
    document.getElementById('e_telefono').innerHTML = "";

    if (document.crea_entrada.sexo.value.length===0){
        document.getElementById('e_sexo').innerHTML = "Tiene que seleccionar su sexo";
        document.crea_entrada.sexo.focus();
        return 0;
    }
    document.getElementById('e_sexo').innerHTML = "";

    if (document.crea_entrada.date_birthday.value.length===0){
        document.getElementById('e_date_birthday').innerHTML = "Tiene que seleccionar su fecha de nacimiento";
        document.crea_entrada.date_birthday.focus();
        return 0;
    }
    document.getElementById('e_date_birthday').innerHTML = "";

    if (document.crea_entrada.country.value.length===0){
        document.getElementById('e_country').innerHTML = "Tiene que seleccionar su pais";
        document.crea_entrada.country.focus();
        return 0;
    }
    document.getElementById('e_country').innerHTML = "";

    if (document.crea_entrada.fllegada.value.length===0){
        document.getElementById('e_fllegada').innerHTML = "Tiene que seleccionar su fecha de llegada";
        document.crea_entrada.fllegada.focus();
        return 0;
    }
    document.getElementById('e_fllegada').innerHTML = "";


    if (document.crea_entrada.fsalida.value.length===0){
        document.getElementById('e_fsalida').innerHTML = "Tiene que seleccionar su fecha de salida";
        document.crea_entrada.fsalida.focus();
        return 0;
    }
    document.getElementById('e_fsalida').innerHTML = "";

    if (document.crea_entrada.tmotivo.value.length===0){
        document.getElementById('e_tmotivo').innerHTML = "Ponga un nombre para su motivo";
        document.crea_entrada.tmotivo.focus();
        return 0;
    }
    document.getElementById('e_tmotivo').innerHTML = "";

    if (document.crea_entrada.motivo.value.length===0){
        document.getElementById('e_motivo').innerHTML = "Tiene que decir un motivo";
        document.crea_entrada.motivo.focus();
        return 0;
    }
    document.getElementById('e_motivo').innerHTML = "";

    document.crea_entrada.submit();
    document.crea_entrada.action="index.php?page=controller_entrada&op=create";

}

function validate_entrada_update() {
    console.log(document.update.nombre.value);

    if (document.update.nombre.value.length===0){
        document.getElementById('e_nombre').innerHTML = "Tiene que escribir su nombre";
        document.update.nombre.focus();
        return 0;
    }
    document.getElementById('e_nombre').innerHTML = "";
    
    if (document.update.apellidos.value.length===0){
        document.getElementById('e_apellidos').innerHTML = "Tiene que escribir sus apellidos";
        document.update.apellidos.focus();
        return 0;
    }
    document.getElementById('e_apellidos').innerHTML = "";
    
    if (document.update.email.value.length===0){
        document.getElementById('e_email').innerHTML = "Tiene que escribir su email";
        document.update.email.focus();
        return 0;
    }
    document.getElementById('e_email').innerHTML = "";

    if (document.update.dni.value.length===0){
        document.getElementById('e_dni').innerHTML = "Tiene que escribir su dni";
        document.update.dni.focus();
        return 0;
    }
    document.getElementById('e_dni').innerHTML = "";

    if (document.update.telefono.value.length===0){
        document.getElementById('e_telefono').innerHTML = "Tiene que escribir su telefono";
        document.update.telefono.focus();
        return 0;
    }
    document.getElementById('e_telefono').innerHTML = "";

    if (document.update.sexo.value.length===0){
        document.getElementById('e_sexo').innerHTML = "Tiene que seleccionar su sexo";
        document.update.sexo.focus();
        return 0;
    }
    document.getElementById('e_sexo').innerHTML = "";

    if (document.update.date_birthday.value.length===0){
        document.getElementById('e_date_birthday').innerHTML = "Tiene que seleccionar su fecha de nacimiento";
        document.update.date_birthday.focus();
        return 0;
    }
    document.getElementById('e_date_birthday').innerHTML = "";

    if (document.update.country.value.length===0){
        document.getElementById('e_country').innerHTML = "Tiene que seleccionar su pais";
        document.update.country.focus();
        return 0;
    }
    document.getElementById('e_country').innerHTML = "";

    if (document.update.fllegada.value.length===0){
        document.getElementById('e_fllegada').innerHTML = "Tiene que seleccionar su fecha de llegada";
        document.update.fllegada.focus();
        return 0;
    }
    document.getElementById('e_fllegada').innerHTML = "";


    if (document.update.fsalida.value.length===0){
        document.getElementById('e_fsalida').innerHTML = "Tiene que seleccionar su fecha de salida";
        document.update.fsalida.focus();
        return 0;
    }
    document.getElementById('e_fsalida').innerHTML = "";

    if (document.update.tmotivo.value.length===0){
        document.getElementById('e_tmotivo').innerHTML = "Ponga un nombre para su motivo";
        document.update.tmotivo.focus();
        return 0;
    }
    document.getElementById('e_tmotivo').innerHTML = "";

    if (document.update.motivo.value.length===0){
        document.getElementById('e_motivo').innerHTML = "Tiene que decir un motivo";
        document.update.motivo.focus();
        return 0;
    }
    document.getElementById('e_motivo').innerHTML = "";

    document.update.submit();
    document.update.action="index.php?page=controller_entrada&op=update";
}

$(document).ready(function () {
        $('.Button_blue1').click(function () {
            //alert("hola");
            var id = this.getAttribute('id');
            //alert(id);

            $.get("module/entrada/controller/controller_entrada.php?op=read_modal&modal=" + id, function (data, status) {
                // console.log(data);

                 
                var json = JSON.parse(data);
                console.log(json);
               
                if(json === 'error') {
                    //console.log(json);
                    //pintar 503
                    window.location.href='index.php?page=503';
                }else{

                    //console.log(json.entrada);
                    $("#nombre").html(json.nombre);
                    $("#apellidos").html(json.apellidos);
                    $("#email").html(json.email);
                    $("#dni").html(json.dni);
                    $("#telefono").html(json.telefono);
                    $("#sexo").html(json.sexo);
                    $("#date_birthday").html(json.date_birthday);
                    $("#country").html(json.country);
                    $("#fllegada").html(json.fllegada);
                    $("#fsalida").html(json.fsalida);
                    $("#tmotivo").html(json.tmotivo);
                    $("#motivo").html(json.motivo);
                    
                    //alert(json.apellidos);

                    $("#modal").show();
                    
                    $("#entrada_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 500, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "down", //<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "explode",
                            duration: 1000
                        }
                    });
                }//end-else
            });
        });
    });