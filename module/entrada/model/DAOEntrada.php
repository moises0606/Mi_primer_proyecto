<?php
    //include("model/connect.php");
   
    $path = $_SERVER['DOCUMENT_ROOT']. '/programacio/workspace_moises/8_MVC_CRUD/';
    include($path . "model/connect.php");

	class DAOEntrada{
		function insert_user($datos){
			$nombre=$datos[nombre];
        	$apellidos=$datos[apellidos];
        	$email=$datos[email];
        	$dni=$datos[dni];
        	$telefono=$datos[telefono];
            $sexo=$datos[sexo];
        	$date_birthday=$datos[date_birthday];
        	$country=$datos[country];
            $fllegada=$datos[fllegada];
            $fsalida=$datos[fsalida];
            $tmotivo=$datos[tmotivo];
            $motivo=$datos[motivo];

        	$sql = " INSERT INTO entradas_8 (nombre, apellidos, email, dni, telefono, sexo, date_birthday, country, fllegada, fsalida, tmotivo, motivo, hora, fecha) VALUES ('$nombre', '$apellidos', '$email', '$dni', '$telefono', '$sexo', '$date_birthday', '$country', '$fllegada', '$fsalida', '$tmotivo', '$motivo', now(), now())";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}
		
		function select_all_user(){
			$sql = "SELECT * FROM entradas_8 ORDER BY id ASC";

			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
		
		function select_user($id){
            $sql = "SELECT * FROM entradas_8 WHERE id='$id'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
		}
		
		function update_user($datos){
            $id=$_GET['id'];
			$nombre=$datos[nombre];
            $apellidos=$datos[apellidos];
            $email=$datos[email];
            $dni=$datos[dni];
            $telefono=$datos[telefono];
            $sexo=$datos[sexo];
            $date_birthday=$datos[date_birthday];
            $country=$datos[country];
            $fllegada=$datos[fllegada];
            $fsalida=$datos[fsalida];
            $tmotivo=$datos[tmotivo];
            $motivo=$datos[motivo];
        	
        	$sql = " UPDATE entradas_8 SET nombre='$nombre', apellidos='$apellidos', email='$email', dni='$dni', telefono='$telefono', sexo='$sexo',"
        		. " date_birthday='$date_birthday', country='$country', fllegada='$fllegada', fsalida='$fsalida', tmotivo='$tmotivo', motivo='$motivo' WHERE id='$id'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}
		
		function delete_user($id){
			$sql = "DELETE FROM entradas_8 WHERE id='$id'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
	
        function isthere($dni){
            $sql = "SELECT * FROM entradas_8 WHERE dni='$dni'";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }
    }