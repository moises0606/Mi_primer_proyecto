<?php
    function validate_entrada(){
        $error='';
        $filtro = array(
            'nombre' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^\D{3,20}$/')
            ),
            
            'apellidos' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
            ),
            
            'email' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'validatemail'
            ),
            
            'dni' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'validar_dni'
            ),

            'telefono' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[9|6|7][0-9]{8}$/')
            ),
            
            'date_birthday' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
            ),

            'fsalida' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'fsalida'
            ),

            'tmotivo' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^\D{3,15}$/')
            ),

            'motivo' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^\D{20,300}$/')
            ),
        );      

        $resultado=filter_input_array(INPUT_POST,$filtro);

        if(!$resultado['nombre']){
            $error['nombre']='Nombre debe tener de 3 a 30 caracteres, no puede tener numeros';
        }elseif(!$resultado['apellidos']){
            $error['apellidos']='Apellidos debe tener de 4 a 120 caracteres';
        }elseif(!$resultado['email']){
            $error['email']='El email debe contener de 5 a 40 caracteres y debe ser un email valido';
        }elseif(!$resultado['dni']){
            $error['dni']='El dni debe ser valido';
        }elseif(!$resultado['telefono']){
            $error['telefono']='El telefono debe tener 9 digitos';
        }elseif(!$resultado['date_birthday']){
            $error['date_birthday']='Formato fecha dd/mm/yy';
        }elseif(!$resultado['fsalida']){
            $error['fsalida']='Debe ser posterior a la de llegada';
        }elseif(!$resultado['tmotivo']){
            $error['tmotivo']='El titulo del motivo debe tener de 3 a 15 caracteres';
        }elseif(!$resultado['motivo']){
            $error['motivo']='El motivo debe tener de 20 a 300 caracteres';
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
    };

    function validatemail($email){
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,40}$/')))){
                    return $email;
                }
            }
            return false;
    }

    function validar_dni($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni, 0, -1);
    if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
        return $dni;
    }else{
        return false;
    } 
}

    function fsalida($fsalida) {
        if ((strtotime($_POST['fsalida']))>(strtotime($_POST['fllegada'])))
            return $fsalida;
        elseif ((strtotime($_POST['fsalida']))===(strtotime($_POST['fllegada'])))
            return $fsalida;
        else
            return false;
    }
?>