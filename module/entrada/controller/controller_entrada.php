<?php
    //include ("module/entrada/model/DAOEntrada.php");

    $path = $_SERVER['DOCUMENT_ROOT']. '/programacio/workspace_moises/8_MVC_CRUD/';
    include($path . 'module/entrada/model/DAOEntrada.php');
    
    if (!isset($_SESSION))
        session_start();

    switch($_GET['op']){
        case 'list';
           try{
                $DAOEntrada = new DAOEntrada();
                $rdo = $DAOEntrada->select_all_user();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/entrada/view/list_entrada.php");
            }
            break;
            
          //  $DAOEntrada = new DAOEntrada();
            //    $rdo = $DAOEntrada->select_all_user();
           // include("module/user/view/list_user.php");
           // break;
        case 'create';
            include("module/entrada/model/validate.php");

            $check = true;

            if (isset($_POST['create'])){
                $check=validate_entrada();
                $error=$check['error'];
                $rdo=' ';
                if ($check){
                    if (!$error){
                        $_SESSION['entrada']=$_POST;
                        try{
                            $DAOEntrada = new DAOEntrada();
                            $rdo2 = $DAOEntrada->isthere($_POST['dni']);
                            if (isset($_POST['create'])&&(!$rdo2)){
                                $rdo = $DAOEntrada->insert_user($_POST);
                                $_POST="0";
                            }else{

                            }
                        }catch (Exception $e){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }

                        if(($rdo)&&(!$rdo2)){
                            $callback = 'index.php?page=controller_entrada&op=list';
                            echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }elseif ($rdo2){
                            echo '<script language="javascript">alert("Solo 1 entrada por persona")</script>';
                        }else {
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }
                }
            }
            include("module/entrada/view/create_entrada.php");
            
            break;
            
        case 'update';
            include("module/entrada/model/validate.php");
            $check = true;

            if (isset($_POST['updater'])){

                $check=validate_entrada();
                $error=$check['error'];

                if ($check){
                    if (!$error){
                        $_SESSION['entrada']=$_POST;

                        try{
                            $DAOEntrada = new DAOEntrada();
                            $rdo = $DAOEntrada->update_user($_POST);
                            $_POST="0";
                        }catch (Exception $e){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                        
                        if($rdo){
                            $callback = 'index.php?page=controller_entrada&op=list';
                            die('<script>window.location.href="'.$callback .'";</script>');
                            echo '<script language="javascript">alert("Actualizado en la base de datos correctamente")</script>';
                        }else{
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }
                }
            }
            
            try{
                $DAOEntrada = new DAOEntrada();
                $rdo = $DAOEntrada->select_user($_GET['id']);
                $entrada=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/entrada/view/update_entrada.php");
            }

            break;
            
        case 'read';
            try{
                $DAOEntrada = new DAOEntrada();
                $rdo = $DAOEntrada->select_user($_GET['id']);
                $entrada=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                $_SESSION['op']="read";
                $_SESSION['entrada']=$entrada;
                //include("module/user/view/read_user.php");
                die('<script>window.location.href="index.php?page=controller_entrada&op=list";</script>');

            }
            break;
        case 'read_modal':
            try{
                $DAOEntrada = new DAOEntrada();
                $rdo = $DAOEntrada->select_user($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $entrada=get_object_vars($rdo);
                echo json_encode($entrada);
                //echo json_encode("error");
                exit;
            }
            break;

        case 'delete';
                $DAOEntrada = new DAOEntrada();
                $rdo = $DAOEntrada->select_user($_GET['id']);
                $entrada=get_object_vars($rdo);

            if (isset($_POST['delete'])){
                try{
                    $DAOEntrada = new DAOEntrada();
                    $rdo = $DAOEntrada->delete_user($_GET['id']);
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
                
                if($rdo){
                    echo '<script language="javascript">alert("Borrado en la base de datos correctamente")</script>';
                    $callback = 'index.php?page=controller_entrada&op=list';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }else{
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
            }
            
            include("module/entrada/view/delete_entrada.php");
            break;

        default;
            include("view/inc/error404.php");
            break;
}